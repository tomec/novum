# Novum Discord Theme ***(this project has been moved to [github](https://github.com/przemec/Novum))***

### Big custom theme with couple of small addons.

To use theme, You need to download a Discord extension called [BetterDiscord](https://betterdiscord.app/) and then, after installation, put [Novum.theme.css](https://github.com/przemec/Novum/blob/master/Novum.theme.css) into "themes" folder (it can be found in BetterDiscord settings).

## Preview

![Preview](https://i.imgur.com/YNQ1LFc.png)
*Original (not edited) background comes from [Nova](https://github.com/din0s/discord-theme/) theme made by [din0s](https://github.com/din0s/).*
